/* Requisição assíncrona para cadastrar um novo user */
const postUsers = async (name, date, email, password) => {
    try {
        const response = await fetch("http://localhost:3000/users", {
            method: 'POST',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: `${name}`,
                date: `${date}`,
                email: `${email}`,
                password: `${password}`
            })
        })

        const content = await response.json()

        return content

    } catch(error) {
        console.log(error)
    }
}

export default postUsers