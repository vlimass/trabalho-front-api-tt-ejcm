import postUsers from "./postUser.js"

/* Coletando os elementos da DOM */
const inputName = document.querySelector("#name")
const inputDate = document.querySelector("#date")
const inputEmail = document.querySelector("#email")
const inputPassword = document.querySelector("#password")
const signUpBtn = document.querySelector("#button")
const errorSpan = document.querySelector(".error")

/* Disparar evento para cadastrar user*/
signUpBtn.addEventListener('click', async (event) => {
  event.preventDefault()
  
  const name = inputName.value
  const date = inputDate.value
  const email = inputEmail.value
  const password = inputPassword.value

  /* Verifica se há campos vazios */
  if(name === "" || date === "" || email === "" || password === "") {
    errorSpan.classList.remove("invisible")
  } else {
    errorSpan.classList.add("invisible")
    await postUsers(name, date, email, password)
  }

})
