# API Método POST de Cadastro de Usuário (TT 2022.1)

## 📓 Sobre o projeto

O projeto se trata de um trabalho realizado durante o **Treinamento Técnico EJCM 2022.1**, em que foi criado um cadastro de usuário da Landing Page Civitas, recolhendo em uma API de users os seus dados de nome, data de nascimento, email e senha.


<hr><br>

## 🛠 Tecnologias e ferramentas

- [JavaScript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [Fetch API](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API)
- [JSON Server](https://www.npmjs.com/package/json-server)

<hr><br>

## 💻 Como rodar o projeto?

<br>

#### ➡️ Clone esse repositório

```
git clone https://gitlab.com/vlimass/trabalho-front-api-tt-ejcm.git
```

#### ➡️ Entre na pasta do projeto

```
cd trabalho-front-api-tt-ejcm
```

#### ➡️ Abra o terminal (pode ser pelo VSCode) e execute o comando para instalar as dependências

```
npm install
```

#### ➡️ Execute o comando para iniciar a API

```
json-server --watch db.json
```

- Se esse comando não rodar, tente:

```
npx json-server --watch db.json
```

#### ➡️ Abra o arquivo `index.html` para iniciar a aplicação (é possível utilizar a extensão Live Server do VSCode para isso)

<br>

![Abrir o arquivo index.html](assets/imagem1-readme-api-cadastro.png)

<br>

#### ➡️ Preencha os dados de um usuário na página da Landing Page do Civitas

<br>

![Preencher cadastro de usuário](assets/imagem2-readme-api-cadastro.png)

<br>

#### ➡️ Verifique se o usuário foi adicionado a API no arquivo `db.json`

<br>

![Verificar se o usuário foi adicionado a API](assets/imagem3-readme-api-cadastro.png)

<br>

#### ➡️ Para parar de servir a API, basta executar o comando `CTRL + C` no terminal

<br><hr>

<div align="center">
MIT License<br>
Copyright © 2022<br> 
Made with ❤️ by <b>Vinícius Lima</b> ✨
</div>
